#!/usr/bin/env bash

rootDir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../")"
buildDir="$rootDir/dist"

usage() {
    cat << EOF
build.sh [options]
Additional options:
  --sourceMap       true if source map should be generated, false if not.
EOF
}

sourceMap=""
while [[ "$#" != "0" ]]; do
    case "$1" in
        --sourceMap)
            if [[ "$2" == "" ]]; then
                echo "$1 requires argument."
                exit 1
            fi
            sourceMap="$2"
            shift
            ;;

        "")
            ;;

        help)
            usage
            exit 0
            ;;

        *)
            echo "Unknown argument $1"
            usage
            exit 1
            ;;
    esac

    shift
done

cd "$rootDir" || exit 1
args=()
if [[ "$sourceMap" != "" ]]; then
    args+=("--sourceMap" "$sourceMap")
fi

rm -rf "$buildDir"
yarn --silent tsc "${args[@]}" || exit 1
cp "$rootDir/src/template.handlebars" "$buildDir" || exit 1
chmod +x "$buildDir/index.js" || exit 1
