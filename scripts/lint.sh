#!/usr/bin/env bash

rootDir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../")"

usage() {
    cat << EOF
lint.sh [options]
Additional options:
 --fmt      Attempt to format linted files.
EOF
}

format="0"
while [[ "$#" != "0" ]]; do
    case "$1" in
        --fmt)
            format="1"
            ;;

        "")
            ;;

        *)
            echo "Unknown argument $1"
            exit 1
            ;;
    esac

    shift
done

echo "Prettier"
cd "$rootDir" || exit 1
if [[ "$format" == "1" ]]; then
    yarn --silent prettier --write "src/**/*.ts" "test/**/*.ts" || exit 1
else
    yarn --silent prettier --check "src/**/*.ts" "test/**/*.ts" || exit 1
fi

echo "Running eslint."
cd "$rootDir" || exit 1
if [[ "$format" == "1" ]]; then
    yarn --silent eslint --max-warnings 0 --fix --ext .ts "src" "test" || exit 1
else
    yarn --silent eslint --max-warnings 0 --ext .ts "src" "test" || exit 1
fi
