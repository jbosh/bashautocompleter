#!/usr/bin/env bash

rootDir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../../")"
scriptDir="$rootDir/scripts"

"$scriptDir/ci/ci.sh"

cd "$rootDir" || exit 1
yarn pack || exit 1

