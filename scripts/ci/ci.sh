#!/usr/bin/env bash

rootDir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../../")"
scriptDir="$rootDir/scripts"

cd "$rootDir" || exit 1
yarn install || exit 1
"$scriptDir/lint.sh" || exit 1
"$scriptDir/build.sh" --sourceMap false || exit 1
"$scriptDir/test.sh" --ci || exit 1

