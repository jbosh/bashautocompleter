#!/usr/bin/env bash

rootDir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../../")"
scriptDir="$rootDir/scripts"

apt update && apt install -y jq || exit 1
"$scriptDir/ci/pack.sh"

cd "$rootDir" || exit 1

echo "Publishing $CI_COMMIT_TAG"

package="$(jq ".version |= \"$CI_COMMIT_TAG\"" "$rootDir/package.json" || exit 1)"
echo "$package" > "$rootDir/package.json"
echo "@${CI_PROJECT_ROOT_NAMESPACE}:registry=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/" >> "$rootDir/.npmrc"
echo "//${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}" >> "$rootDir/.npmrc"
yarn publish || exit 1

