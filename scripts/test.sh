#!/usr/bin/env bash

rootDir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../")"

usage() {
    cat << EOF
test.sh [options]
Additional options:
  --forceColors
  --forceExit

EOF
}

forceColors="0"
forceExit="0"
while [[ "$#" != "0" ]]; do
    case "$1" in
        --forceColors)
            forceColors="1"
            ;;

        --forceExit)
            forceExit="1"
            ;;

        --ci)
            # Hidden option to enable a bunch of things.
            forceExit="1"
            forceColors="1"
            ;;

        "")
            ;;

        *)
            echo "Unknown argument $1"
            exit 1
            ;;
    esac

    shift
done

args=()

if [[ "$forceExit" == "1" ]]; then
    args+=("--forceExit")
fi

if [[ "$forceColors" == "1" ]]; then
    args+=("--colors")
fi

cd "$rootDir" || exit 1
yarn --silent jest "test" "${args[@]}" || exit 1
