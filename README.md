# Bash Autocompleter
Bash autocompleter is a command line utility to generate autocomplete scripts for bash.

The syntax for schemas is loosely based on [completely](https://github.com/DannyBen/completely).

## Install
```shell
npm i -g bash-autocompleter
```

## Using `bash-autocompleter` command line
Bash autocompleter will generate shell script either to a file or stdout based on json or yaml schema.

```shell
bash-autocompleter schema.yaml -o autocomplete.sh
```

An example yaml schema:
```yaml
mygit:
- --help
- --version
- status
- init
- commit

mygit status:
- --help
- --verbose
- --branch
- $(git branch 2> /dev/null)

mygit diff:
  - --cached
  - --no-index
  - <file>
  - $(git branch 2> /dev/null)

# Add files after double dash operator. It should be lowest priority of wildcard `--`.
mygit diff*--*: <file>

mygit init:
- --bare
- <directory>

mygit commit:
- <file>
- --help
- --message
- --all
- -a
- --quiet
- -q
```

Order of precedence in the schema file is top to bottom. If there are conflicts, the topmost schema will be taken first.

### OSX and Bash 3
If you're running on OSX or a very old version of bash you can use:
```shell
bash-autocompleter schema.yaml --bash3 -o autocomplete.sh
```
This will generate bash3 compatible scripts.


## Wildcards
It's possible to use wildcards to signify what longer commands would look like. For example, below you can see what the autocomplete command for `bash-autocompleter schema.yml -o<tab>` would look like in the `
bash-autocompleter*-o` section.

```yaml
bash-autocompleter:
- <file>
- -o
- --bash3

bash-autocompleter*-o:
- <file>
```

## Empty arguments
If you have an argument that has no suggestion, use `<empty>`. For example, in git, the force flag doesn't have any followup options so would use `<empty>`.

```yaml
git*--force: <empty>

git*-f:
  - <empty>
```

## References
Sometimes it's a lot of effort to duplicate flags that have the same content. For these, you can use references.

```yaml
build*--config: &build-test
  - debug
  - release

build*-c: &build-test
```

The above code will make `build -c<tab>` and `build --config<tab>` identical.
