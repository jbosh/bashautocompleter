import * as yaml from "js-yaml";
import fs from "fs";
import Path from "path";
import handlebars from "handlebars";

let handlebarsTemplate = fs.readFileSync(Path.join(__dirname, "template.handlebars"), "utf8");
let rootTemplate = handlebars.compile(handlebarsTemplate, {
    noEscape: true,
});

interface RootData {
    functionName: string;
    command: string;
    commands: AutocompleteCommand[];
    bash3: boolean;
    autocomplete: {
        file: boolean;
        dir: boolean;
    };
}

interface AutocompleteCommand {
    pattern: string;
    action: string;
    raw: boolean;
}

function getSpecialAction(action: string): string {
    switch (action) {
        case "<empty>":
            return "COMPREPLY=()";
        case "<file>":
            return 'fileAutocomplete "$cur"';
        case "<dir>":
            return 'dirAutocomplete "$cur"';
        default:
            console.error(`Invalid action ${action}`);
            throw "Invalid action";
    }
}

export function compile(schemaString: string, options?: { bash3?: boolean }): string {
    options ??= {};
    let bash3 = options.bash3 ?? false;

    let schema: any;
    if (schemaString.trim().startsWith("{")) {
        try {
            schema = JSON.parse(schemaString);
        } catch (e: any) {
            console.error("Failed to read data as json.");
            console.error(JSON.stringify(e, null, "  "));
            throw e;
        }
    } else {
        try {
            schema = yaml.load(schemaString);
        } catch (e: any) {
            console.error("Failed to read data as yaml.");
            console.error(JSON.stringify(e, null, "  "));
            throw e;
        }
    }

    let allKeys = new Set(Object.keys(schema).map((k) => k.replace(/ .+$/g, "")));
    if (allKeys.size !== 1) {
        console.error("All patterns must start with the same word.");
        throw "All patterns must start with the same word.";
    }

    let autocomplete = {
        file: false,
        dir: false,
    };

    let commands: AutocompleteCommand[] = [];
    for (let key of Object.keys(schema).reverse()) {
        let value = schema[key] as string | string[];
        value ??= "";

        let match = key.match(/^[^ ]+ +(.+)/);
        let pattern = "";
        if (match) {
            pattern = match[1] ?? "";
        }

        if (pattern.indexOf("*") >= 0) {
            pattern = `'${pattern.replace(/\*/g, "'*'")}'`;
        } else {
            pattern = `'${pattern}'*`;
        }

        let raw = false;
        let action;
        if (Array.isArray(value) && value.length === 1 && value[0].match(/^<.+>$/)) {
            action = getSpecialAction(value[0]);
            raw = true;
            autocomplete.dir ||= value[0] === "<dir>";
            autocomplete.file ||= value[0] === "<file>";
        } else if (!Array.isArray(value) && value.match(/^<.+>$/)) {
            action = getSpecialAction(value);
            raw = true;
            autocomplete.dir ||= value === "<dir>";
            autocomplete.file ||= value === "<file>";
        } else {
            let joined = Array.isArray(value) ? value.join(" ") : value;
            if (joined === "") {
                action = getSpecialAction("<empty>");
                raw = true;
            } else {
                action = `compgen -W "${joined}" -- "$cur"`;
            }
        }

        commands.push({
            pattern,
            action,
            raw,
        });
    }

    let command = allKeys.values().next().value as string;
    let functionName = `__autocomplete_${command.replace(/[./]/g, "_")}`;
    let rootData: RootData = {
        functionName,
        command,
        commands,
        bash3,
        autocomplete,
    };

    return rootTemplate(rootData);
}
