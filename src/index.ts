#!/usr/bin/env node

import { program } from "commander";
import * as fs from "fs";
import { compile } from "./compile";

program
    .option("-o, --output <string>", "Path to output file. If this is left empty, output is sent to stdout.")
    .option("--bash3", "Force generation of bash3 compatible autocomplete scripts. This is useful for OSX.", false)
    .argument("<input>", "Path to input file.");

program.parse();

interface CommandLineArgs {
    input: string;
    output?: string;
    bash3: boolean;
}

let args = program.opts<CommandLineArgs>();
args.input = program.args[0];

function main() {
    let fileData;
    try {
        fileData = fs.readFileSync(args.input, "utf8");
    } catch (e: any) {
        console.error(JSON.stringify(e, null, "  "));
        return 1;
    }

    let result;
    try {
        result = compile(fileData, { bash3: args.bash3 });
    } catch {
        return 1;
    }

    if (args.output) {
        fs.writeFileSync(args.output, result, "utf8");
    } else {
        process.stdout.write(result + "\n");
    }

    return 0;
}

process.exitCode = main();
