import { describe } from "node:test";
import { runner } from "./lib/runner";
import * as Path from "path";

describe("Basic", () => {
    test.concurrent("Single Layer", async () => {
        let schemaPath = Path.join(__dirname, "data", "basic-single-layer.yml");
        await runner({
            schemaPath,
            args: ["--q"],
            expected: ["--quiet", "--quick"],
        });

        await runner({
            schemaPath,
            args: ["r"],
            expected: ["run"],
        });

        await runner({
            schemaPath,
            args: ["super-test"],
            expected: ["super-test"],
        });

        await runner({
            schemaPath,
            args: ["super-test", "run", ""],
            expected: ["--quick", "--quiet", "-q", "run", "super-test"],
        });
    });

    test.concurrent("External Script", async () => {
        let schemaPath = Path.join(__dirname, "data", "basic-external-script.yml");
        await runner({
            schemaPath,
            args: ["--"],
            expected: [""],
        });

        await runner({
            schemaPath,
            args: ["h"],
            expected: ["hello"],
        });

        await runner({
            schemaPath,
            args: ["world"],
            expected: ["world"],
        });

        await runner({
            schemaPath,
            args: ["o"],
            expected: ["one"],
        });

        await runner({
            schemaPath,
            args: ["t"],
            expected: ["two", "three"],
        });
    });
});
