import { describe } from "node:test";
import { runner } from "./lib/runner";
import * as Path from "path";

describe("Json", () => {
    test.concurrent("Single Layer", async () => {
        let schemaPath = Path.join(__dirname, "data", "basic-single-layer.json");
        await runner({
            schemaPath,
            args: ["--q"],
            expected: ["--quiet", "--quick"],
        });

        await runner({
            schemaPath,
            args: ["r"],
            expected: ["run"],
        });

        await runner({
            schemaPath,
            args: ["super-test"],
            expected: ["super-test"],
        });

        await runner({
            schemaPath,
            args: ["super-test", "run", ""],
            expected: ["--quick", "--quiet", "-q", "run", "super-test"],
        });
    });
});
