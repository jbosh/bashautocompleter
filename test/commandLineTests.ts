import { describe } from "node:test";
import { spawnAsync } from "./lib/runner";

describe("Command Line", () => {
    test.concurrent("Bash5", async () => {
        let stdout = await spawnAsync("yarn", ["--silent", "ts-node", "src/index.ts", "test/data/basic-single-layer.yml"]);
        expect(stdout).toContain("readarray -t COMPREPLY");
    });

    test.concurrent("Bash3", async () => {
        let stdout = await spawnAsync("yarn", [
            "--silent",
            "ts-node",
            "src/index.ts",
            "test/data/basic-single-layer.yml",
            "--bash3",
        ]);
        expect(stdout).not.toContain("readarray -t COMPREPLY");
    });
});
