import { describe } from "node:test";
import * as Path from "path";
import { runner } from "./lib/runner";

describe("File Command", () => {
    test.concurrent("File", async () => {
        let schemaPath = Path.join(__dirname, "data", "file-command.yml");
        await runner({
            schemaPath,
            args: ["file", "t"],
            expected: ["test", "tsconfig.json"],
            cwd: Path.join(__dirname, ".."),
        });
    });

    test.concurrent("Dir", async () => {
        let schemaPath = Path.join(__dirname, "data", "file-command.yml");
        await runner({
            schemaPath,
            args: ["dir", "t"],
            expected: ["test"],
            cwd: Path.join(__dirname, ".."),
        });
    });
});
