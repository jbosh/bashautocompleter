import * as fs from "fs/promises";
import { compile } from "../../src/compile";
import { spawn, SpawnOptionsWithoutStdio } from "child_process";
import "jest-expect-message";

export function spawnAsync(command: string, args?: ReadonlyArray<string>, options?: SpawnOptionsWithoutStdio): Promise<string> {
    return new Promise<any>((resolve, reject) => {
        let process = spawn(command, args, options);

        let output = "";
        process.stdout.on("data", (data) => {
            output += data;
        });

        process.stderr.on("data", (data) => {
            output += data;
        });

        process.on("close", (code) => {
            if (code !== null && code !== 0) {
                console.error(`${command} ${args?.join(" ")} failed with ${code}`);
                console.error(output);
                reject(new Error(code.toString()));
            } else {
                resolve(output);
            }
        });
    });
}

interface RunnerArgs {
    /**
     * Path to the schema file.
     */
    schemaPath: string;

    /**
     * Name of the script in the schema. Default is `__autocomplete_app`.
     */
    scriptName?: string;

    /**
     * Args to call autocomplete script with.
     */
    args: string[];

    /**
     * Expected results.
     */
    expected: string[];

    /**
     * Current working directory. If not set, it is unchanged.
     */
    cwd?: string;
}

export async function runner(args: RunnerArgs): Promise<void> {
    let scriptName = args.scriptName ?? "__autocomplete_app";

    let schemaString = await fs.readFile(args.schemaPath, "utf8");
    let script = compile(schemaString, { bash3: true });

    script += "\n";
    script += "COMPREPLY=()\n";
    script += `COMP_WORDS=("${scriptName}" ${args.args.map((a) => `"${a}"`).join(" ")})\n`;
    script += `COMP_CWORD="${args.args.length}"\n`;
    script += `COMP_LINE="${scriptName} ${args.args.join(" ")}"\n`;
    script += 'COMP_POINT="${#COMP_LINE}"\n';
    script += `${scriptName} ${args.args.join(" ")}\n`;
    script += "echo ${COMPREPLY[*]}\n";
    let compreply = await spawnAsync("bash", ["-c", script], { cwd: args.cwd });
    let actual = compreply.trim().split(" ").sort();
    let expected = args.expected.sort();
    expect(actual, compreply).toStrictEqual(expected);
}
