import type { Config } from "@jest/types";

const config: Config.InitialOptions = {
    displayName: "BashAutocompleter",
    verbose: true,
    testEnvironment: "node",
    transform: {
        "^.+\\.(ts)$": ["ts-jest", { tsconfig: "tsconfig.json" }],
    },
    moduleFileExtensions: ["ts", "js", "json", "node"],
    testMatch: ["<rootDir>/test/*.ts"],
    setupFilesAfterEnv: ["jest-expect-message", "jest-extended/all"],
};

export default config;
